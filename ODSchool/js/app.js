window.onload = function() {
	
	var a = document.getElementById("link1");
	var b = document.getElementById("link2");
	var c = document.getElementById("link3");
	var d = document.getElementById("link4");
	
	a.onclick = function() {
		var svg = resetSvg(width, height);
		
		$('li').removeClass("active");
		$(this).parent().addClass("active"); 
		
		var valueById = d3.map();
	 
		var scale = d3.scale.log()
			.domain([6, 11021])
			.range([0, 8]);

		queue()
			.defer(d3.json, "be-municipalities.json")
			.defer(d3.tsv, "school.tsv", function(d) { valueById.set(d.id, +d.students); })
			.await(ready);

		function ready(error, be) {
			renderTitle("Schüler","Anzahl der Schüler pro Gemeinde")
			shape = renderShapes(svg, path, be, valueById, scale, "Blues");
			shape.on('mouseover', function(d) { renderInfoName(d); });
			shape.on('mouseout', function() { resetInfo(); } );
			renderBorders(svg, path, be);
			var data = reduceArray(9, scale.ticks());
			renderLegend(svg, data, width, height, colorbrewer.Blues[9], function(d) { return "≥ " + Math.round(d); })
		}
		
		return false;
	}
	
	b.onclick = function() {
		var svg = resetSvg(width, height);
		
		$('li').removeClass("active");
		$(this).parent().addClass("active"); 
		
		var valueById = d3.map();
	 
		var scale = d3.scale.linear()
			.domain([0, 1])
			.range([0, 8]);

		queue()
			.defer(d3.json, "be-municipalities.json")
			.defer(d3.tsv, "school.tsv", function(d) { valueById.set(d.id, +d.womens); })
			.await(ready);

		function ready(error, be) {			
			renderTitle("Geschlecht","Schülerinnen und Schüler pro Gemeinde")
			shape = renderShapes(svg, path, be, valueById, scale, "PiYG");
			shape.on('mouseover', function(d) { 
					format = function(d) { return (Math.round(d.value * 1000) / 10) + " %";};
					renderInfoName(d);
					renderInfoBox(d, valueById, format);
				});
			shape.on('mouseout', function() { resetInfo(); } );
			renderBorders(svg, path, be);
			var data = scale.ticks(9);
			renderLegend(svg, data, width, height, colorbrewer.PiYG[9], function(d) { return "≥ " + d; })
		}
		   
		return false;
	}
	
	c.onclick = function() {
		var svg = resetSvg(width, height);
		
		$('li').removeClass("active");
		$(this).parent().addClass("active"); 
		
		var valueById = d3.map();
	 
		var scale = d3.scale.linear()
			.domain([0, 1])
			.range([0, 8]);

		queue()
			.defer(d3.json, "be-municipalities.json")
			.defer(d3.tsv, "school.tsv", function(d) { valueById.set(d.id, +d.foreigners); })
			.await(ready);

		function ready(error, be) {
			renderTitle("Ausländer","Anzahl ausländischer Schüler pro Gemeinde")
			shape = renderShapes(svg, path, be, valueById, scale, "Reds");
			shape.on('mouseover', function(d) { 
					format = function(d) { return (Math.round(d.value * 1000) / 10) + " %";};
					renderInfoName(d);
					renderInfoBox(d, valueById, format);
				});
			shape.on('mouseout', function() { resetInfo(); } );
			renderBorders(svg, path, be);
			var data = scale.ticks(9);
			renderLegend(svg, data, width, height, colorbrewer.Reds[9], function(d) { return "≥ " + d; })
		}
		
		return false;
	}
	
	d.onclick = function() {
		var svg = resetSvg(width, height);
		
		$('li').removeClass("active");
		$(this).parent().addClass("active"); 
		
		var valueById = d3.map();
	 
		var scale = d3.scale.linear()
			.domain([0, 0.626])
			.range([0, 8]);

		queue()
			.defer(d3.json, "be-municipalities.json")
			.defer(d3.tsv, "school.tsv", function(d) { valueById.set(d.id, +d.foreignlanguages); })
			.await(ready);

		function ready(error, be) {
			renderTitle("Fremdsprachig","Anzahl fremdsprachiger Schüler pro Gemeinde")
			shape = renderShapes(svg, path, be, valueById, scale, "Purples");
			shape.on('mouseover', function(d) { 
					format = function(d) { return (Math.round(d.value * 1000) / 10) + " %";};
					renderInfoName(d);
					renderInfoBox(d, valueById, format);
				});
			shape.on('mouseout', function() { resetInfo(); } );
			renderBorders(svg, path, be);
			var data = calcLinearTicks(9,0,0.626);
			renderLegend(svg, data, width, height, colorbrewer.Purples[9], function(d) { return "≥ " + (d * 100).toFixed(1) + "%"; })
		}
		   
		return false;
	}

	var width = 520,
		height = 520;

    var path = d3.geo.path()
		.projection(matrix(1, 0, 0, 1, -220, -5));
		
	var svg = initSvg(width, height);

	a.onclick();
}

function matrix(a, b, c, d, tx, ty) {
  return d3.geo.transform({
    point: function(x, y) { this.stream.point(a * x + b * y + tx, c * x + d * y + ty); }
  });
}

function reduceArray(factor, array) {
	var length = array.length;
	var step = length / factor;
	var new_array = [];
	for(var i = 0; i < factor; i++) {
		new_array[i] = array[Math.floor(i * step)];
	}
	return new_array;
}

function calcLinearTicks(count, from, to) {
	var distance = to - from;
	var step = distance / count;
	var new_array = [];
	for(var i = 0; i < count; i++) {
		new_array[i] = i * step;
	}
	return new_array;
}

function resetSvg(width, height) {
	d3.select("svg").remove();
	return initSvg(width, height);
}

function resetInfo() {
	$(".infochart").empty();
	$(".infochart").addClass("hidden");
	$(".namelabel").empty().append("Fahre mit der Maus über die Gemeinden");
}

function initSvg(width, height) {	 
	var svg = d3.select("#app")
		.append("svg")
		.style("background-color", "white")
		.attr("width", width)
		.attr("height", height);
		
	return svg;
}

function renderBorders(svg, path, be) {
	svg.append("path")
		.datum(topojson.mesh(be, be.objects.municipalities, function(a, b) { return a !== b; }))
		.attr("class", "municipalities_border")
		.attr("d", path);
}

function renderShapes(svg, path, be, valueById, scale, color) {
	return svg.append("g")
		.attr("class", color)
		.selectAll("path")
		.data(topojson.feature(be, be.objects.municipalities).features)
		.enter()
		.append("path")
		.attr("class", function(d) { 
			var value = valueById.get(d.id); 
			var bucket = Math.round(scale(value));
			if (isNaN(bucket)) {
				return "no_data";
			}
			return "q" + bucket + "-9";
			})
		.attr("d", path);		
}

function renderTitle(title1,title2) {
	$(".title1").empty().append("<h1>"+title1+"</h1>")
	$(".title2").empty().append("<p>"+title2+"</p>")
}

function renderInfoBox(d, valueById, format, scale, color) {
	
	d3.select(".infochart").select("svg").remove();
	$(".infochart").removeClass("hidden");

	var value = valueById.get(d.id);
	var w = 200,h = 200;
	var dataset =[value,1-value];
	var outerRadius = w / 2;
	var innerRadius = 5;
	var pie = d3.layout.pie();
	var color = d3.scale.category10();
	var arc = d3.svg.arc()
		.innerRadius(innerRadius)
		.outerRadius(outerRadius);

	var svg = d3.select(".infochart")
		.append("svg")
		.attr("width", w)
		.attr("height", h);
	
	//Set up groups
	var arcs = svg.selectAll("g.arc")
		.data(pie(dataset))
		.enter()
		.append("g")
		.attr("class", "arc")
		.attr("transform", "translate(" + outerRadius + "," + outerRadius + ")");
	
	//Draw arc paths
	arcs.append("path")
		.attr("fill", function(d, i) {
			return color(i);
		})
		.attr("d", arc);
	
	//Labels
	arcs.append("text")
	    .attr("transform", function(d) {
	    	return "translate(" + arc.centroid(d) + ")";
	    })
	    .attr("text-anchor", "middle")
	    .text(format);
}

function renderInfoName(d) {
	var name = d.properties.name;
	$('.namelabel').empty()
		.append("Name der Gemeinde: " + name)
		.stop(true, true).show(400);
}

function renderLegend(svg, data, width, height, color, format) {
	var legendHeight = 30;
	
	var textTicks = data.length;
	var textWidth = width / textTicks;
	
	var legendElementHeight = 15;
	var legendElementWidth = width / 9;
	
	var legend = svg.selectAll(".legend")
		.data(data, function(d) { return d; })
		.enter().append("g")
		.attr("class", "legend");

	legend.append("rect")
		.attr("x", function(d, i) { return legendElementWidth * i; })
		.attr("y", height - legendHeight)
		.attr("width", legendElementWidth)
		.attr("height", legendElementHeight / 2)
		.style("fill", function(d, i) { return color[i]; });

	legend.append("text")
		.attr("class", "mono")
		.text(format)
		.attr("x", function(d, i) { return textWidth * i; })
		.attr("y", height - legendHeight + (legendElementHeight * 1.5) );
}